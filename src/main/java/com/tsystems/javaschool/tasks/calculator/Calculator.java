package com.tsystems.javaschool.tasks.calculator;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        // Check for null or empty expression
        if (statement == null || statement.isEmpty()) {
            return null;
        }
        // Check for unsupported characters
        if (statement.matches(".*[a-zA-Z,]+.*")) {
            return null;
        }
        // Check for multiple '+', '-', '*', '/' and '.'
        if (statement.matches(".*[/+*.\\-]{2,}+.*")) {
            return null;
        }
        // Use Script Engine for evaluate statement
        ScriptEngineManager manager = new ScriptEngineManager();
        ScriptEngine engine = manager.getEngineByName("javascript");
        try {
            String result = engine.eval(statement).toString();
            // Check for not numeric result
            if (result.matches(".*[a-zA-Z]+.*")) {
                return null;
            }
            return result;
        } catch (Exception e) {
            return null;
        }
    }

}
