package com.tsystems.javaschool.tasks.subsequence;

import java.util.*;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        // Check for null
        if (x == null || y == null) {
            throw new IllegalArgumentException();
        }

        // Check correct lists sizes
        if (x.size() > y.size()) {
            return false;
        }

        Queue xQueue = new LinkedList(x);

        // Filter 'y' so it contains item from 'x' only with same order.
        // If result list length and 'x' length are equal it is possible
        // to get a sequence which is equal to the 'x' by removing
        // some elements from 'y'.
        return y.stream().filter(item -> {
            if (item.equals(xQueue.peek())) {
                xQueue.poll();
                return true;
            }
            return false;
        }).count() == x.size();
    }
}
