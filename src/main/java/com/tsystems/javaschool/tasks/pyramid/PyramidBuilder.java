package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        // Check for null items
        if (inputNumbers.contains(null)) {
            throw new CannotBuildPyramidException();
        }

        /*
          Check for pyramid built possibility.
          Pyramid can be built only if inputNumber.size() is 'Triangular Number'.
          Number 'm' is 'Triangular Number' only if 'm * 8 + 1' is integer square.
         */
        double square = Math.sqrt(8 * inputNumbers.size() + 1);
        if (square - (int) square != 0.) {
            throw new CannotBuildPyramidException();
        }

        Collections.sort(inputNumbers);
        int columnCount = (int) (square - 2);
        int[][] pyramid = new int[(columnCount + 1) / 2][columnCount];
        int[] callCalc = new int[2];

        inputNumbers.forEach(item -> {
            // Calculate item column
            int row = callCalc[0];
            int column = columnCount / 2 - callCalc[0] + callCalc[1]++ * 2;

            // Set item in cell
            pyramid[row][column] = item;

            // If necessary change row
            if (callCalc[1] > callCalc[0]) {
                callCalc[0]++;
                callCalc[1] = 0;
            }
        });

        return pyramid;
    }


}
